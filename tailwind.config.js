const defaultTheme = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  mode: "jit",
  theme: {
    extend: {
      fontFamily: {
        mono: ["VT323", ...defaultTheme.fontFamily.mono]
      },
      colors: {
        primary: colors.green,
        lime: colors.lime
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
